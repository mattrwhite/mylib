# usage: "sudo make install"
# pure Lua, so install is just copying to system folder

INSTALL_PATH=/usr/local/share/lua/5.1

install:
	mkdir -p $(INSTALL_PATH)/mylib
	install -m 644 -t $(INSTALL_PATH)/mylib utils.lua tableutils.lua

uninstall:
	rm -rf $(INSTALL_PATH)/mylib
