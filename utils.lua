-- Other general Lua libraries:
-- * metalua table library has useful array fns like zip, cat, flatten
-- * Penlight
-- Ideas from other languages:
-- * https://github.com/gameclosure/ff

oper = {
  add = function(a,b) return a + b; end;
  sub = function(a,b) return a - b; end;
  mul = function(a,b) return a * b; end;
  div = function(a,b) return a / b; end;
  -- logical ... we could do ["and"] etc, but users must also do ["and"]
  --  i.e., they can't do oper.and
  opand = function(a,b) return a and b; end;
  opor = function(a,b) return a or b; end;
  opnot = function(a) return (not a); end;
  -- comparison
  lt = function(a,b) return a < b; end;
  gt = function(a,b) return a > b; end;
  le = function(a,b) return a <= b; end;
  ge = function(a,b) return a >= b; end;
  eq = function(a,b) return a == b; end;
  neq = function(a,b) return a ~= b; end;
};

function min(...)
  local arg = {...};
  if #arg == 1 and type(arg[1]) == "table" then
    return math.min(unpack(arg[1]));
  elseif #arg > 1 then
    return math.min(unpack(arg));
  else
    error("invalid arguments");
  end
end

function max(...)
  local arg = {...};
  if #arg == 1 and type(arg[1]) == "table" then
    return math.max(unpack(arg[1]));
  elseif #arg > 1 then
    return math.max(unpack(arg));
  else
    error("invalid arguments");
  end
end

function math.round(x)
  return math.floor(x+0.5);
end

function modidx(t, idx)
  -- base-1 tables don't play nice with modulo division
  -- of course, we could use metatables to make a table
  --  look endless
  return t[((idx - 1) % #t) + 1];
end

function eval(s, ...)
  return assert(loadstring(s))(...);
end

--- simple bit set/test/clear (from Rici Lake)

function bit(idx)
  return 2 ^ idx; -- 0-based indexing
end

-- Typical call: if hasbit(x, bit(3)) then ...
function testbit(x, p)
  -- x % 2^(n+1) clears all bits beyond 2^n
  return x % (p + p) >= p;
end

function setbit(x, p)
  return testbit(x, p) and x or x + p;
end

function clearbit(x, p)
  return testbit(x, p) and x - p or x;
end

--- functionals that would be made unnecessary by short anon fn defs

function compose(f,g)
  if f and g then
    return function (...)
      return f(g(...));
    end
  else
    return f or g;
  end
end

function bindl(f, ...)
  local largs = {...};
  return function (...)
    return f(unpack(largs), ...);
  end
end

function bindr(f, ...)
  local rargs = {...};
  return function (...)
    return f(..., unpack(rargs));
  end
end

-- bind all
-- TODO: only bind non-nil args
-- TODO: partial bind for fn that takes single table as arg (i.e., for named args)
function bind(f, ...)
  local allargs = {...};
  return function ()
    return f(unpack(allargs));
  end
end

-- pretty printing ... from http://lua-users.org/wiki/TableSerialization
function to_string(...)

  local function key_to_str(k)
    return type(k) == 'number' and "["..tostring(k).."]" or tostring(k);
  end
  local function val_to_str(s)
    return type(s) == "string" and '"'..s..'"' or tostring(s);
  end

  local function table_print(tt, indent, done)
    done = done or {};  -- guard against infinite recursion for self-referential tables
    indent = indent or 0;
    if type(tt) == "table" then
      local sb = {};
      for key, value in pairs (tt) do
        table.insert(sb, string.rep(" ", indent));  -- indent it
        if type (value) == "table" and not done[value] then
          done[value] = true;
          table.insert(sb, key_to_str(key).." = {\n");
          table.insert(sb, table_print(value, indent + 2, done));
          table.insert(sb, string.rep(" ", indent));  -- indent it
          table.insert(sb, "},\n");
        else
          table.insert(sb, key_to_str(key).." = "..val_to_str(value)..",\n");
        end
      end
      return table.concat(sb);
    else
      return tt.."\n";
    end
  end

  local res = {};
  for ii,tbl in ipairs({...}) do
    if type(tbl) == 'table' then
      table.insert(res, "{\n"..table_print(tbl).."}");
    elseif type(tbl) == 'string' then
      table.insert(res, '"'..tbl..'"');
    else
      table.insert(res, tostring(tbl));
    end
  end
  return table.concat(res, "\t");
end

-- our old function
function dump(t)
 print(to_string(t));
end

-- split string on pattern sep
function string.split(s, sep)
  sep = sep or " ";
  local res = {};
  local a, b, c = 0, 0, 0;
  repeat
    c = b + 1
    a, b = string.find(s, sep, c);
    table.insert(res, string.sub(s, c, (a or 0)-1));
  until not a
  return res;
end

-- trim string ... trim11 from http://lua-users.org/wiki/StringTrim
function string.trim(s)
 local n = s:find("%S");
 return n and s:match(".*%S", n) or "";
end

-- use io.write so that a newline isn't automatically appended
function printf(s,...)
  return io.write(s:format(...))
end -- function

sprintf = string.format;

-- read an entire file into a string (returns nil, error message on failure)
function io.readAll(filename)
  local file = io.open(filename, "rb");
  if not file then
    return nil, "error opening "..filename;
  end
  local contents = file:read("*a");
  file:close();
  return contents;
end

-- build path from components; removes all leading (except first) and trailing '/'s, then concats with '/',
--  so result never has trailing '/'
function io.path(...)
  local args = {...};
  for ii,s in ipairs(args) do
    if ii > 1 then
      s = string.gsub(s, "^%s*%/", "");
    end
    args[ii] = string.gsub(s, "%/%s*$", "");
  end
  return table.concat(args, "/");
end


-- Iterators

-- iterate over table t based on the order of the keys,
--  as determinined by sortfn
-- from PIL, 19.3
function pairsByKeys (t, sortfn)
  local a = {}
  -- create array where values are the keys of t
  for n in pairs(t) do table.insert(a, n) end
  table.sort(a, sortfn)
  local ii = 0      -- iterator variable
  local iter = function ()   -- iterator function
    ii = ii + 1
    if a[ii] == nil then return nil
    else return a[ii], t[a[ii]]
    end
  end
  return iter
end

-- return an iterator with specified number of items skipped
-- see PIL 7.2
function iterskip(_f, _s, _var, count)
  -- default is to skip one (first) item
  for ii = 1,(count or 1) do
    _var = _f(_s, _var);
  end
  return _f, _s, _var;
end


--- Reversible wrapping (using metatables)
-- Another option: wrapper is a fn; returns origfn if passed special arg

-- wrapper function should take target fn as first arg
function wrapfn(wrapper, fn)
  local mt = {
    origfn = fn,
    __call = function (tbl, ...)
      return wrapper(fn, ...);
    end
  }
  return setmetatable({}, mt);
end

function unwrapfn(fn)
  local mt = getmetatable(fn);
  if mt and mt.origfn then
    return mt.origfn;
  else
    return fn;
  end
end

-- wrap/unwrap entire table of functions
function wraptable(wrapper, t)
  local mt = {
    origtable = t,
    __index = function (tbl,key)
      return wrapfn(wrapper, t[key]);
    end,
    -- Allow addition of new functions to table
    __newindex = t
  }
  return setmetatable({}, mt);
end

function unwraptable(t)
  local mt = getmetatable(t);
  if mt and mt.origtable then
    t = mt.origtable;
  end
  return t;
end
