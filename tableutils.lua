-- table and array utilities
--
-- See Mathematica for many, many useful functions
-- Misc ideas: option for recursive application of map, filter, etc on
--  non-flat trees
-- In general, most of these fns return a new table, as this is the only
--  natural thing to do; there are few sketchy cases like array.rev().
--  Of course, this represents only a shallow copy.
-- Since this library is called "table", first arg to each fn should
--  be the table (as opposed to other args)

-- thoughts on documenting functions:
--  table._doc = {}
--  table.union = function ... end
--  table._doc.union = [[ table.union(t1, t2) \n Returns new table containing ... ]]
-- To display: doc('table.union'), doc(table, 'union')
-- I think we'd need metatables for doc(table.union) to work
-- What about function metadata?
--  table._props.union = {const=true, minargs=2, maxargs=2, arg1='table', arg2='table'};
-- For testing, we could wrap every fn with a verifier that examines _props
--  we could verify const by wrapping every input with a metatable

-- The proper way to convert binary fns like union and merge to handle more args is array.reduce({...}, table.union)

--local _path = (...):match("(.-)[^%.%/]+$") or "";  -- module is passed its file path
--require(_path..'utils');

array = {};

-- make a copy of a table
-- Note that keys which are tables are not duplicated
function table.copy(t, maxdepth, tblstack)
  maxdepth = maxdepth or 1;
  if maxdepth <= 0 then return t; end

  local yy = {};
  -- prevent infinite recursion for self-referential tables
  if not tblstack then
    tblstack = {};
  end
  tblstack[t] = true;

  for key,val in pairs(t) do
    if type(val) == "table" and not tblstack[val] then
      yy[key] = table.copy(val, maxdepth - 1, tblstack);
    else
      yy[key] = val;
    end
  end
  return yy;
end

-- 100 should be deep enough...
function table.deepcopy(t)
  return table.copy(t, 100);
end

function table.head(t)
  local key,val = next(t, nil);
  return val,key;
end

-- insert elements from second table into first
-- Example: table.merge(_G, table) to bring table methods into
--  global namespace
function table.merge(t1, t2)
  for key,val in pairs(t2) do
    t1[key] = val;
  end
  return t1;
end

function table.deepmerge(dest, src)
  for k,v in pairs(src) do
    if type(v) == "table" and dest[k] and type(dest[k]) == "table" then
      if #v > 0 or #dest[k] > 0 then
        array.join(dest[k], v);  -- concat arrays
      else
        table.deepmerge(dest[k], v);  -- deep merge non-array tables
      end
    else
      dest[k] = v;
    end
  end
  return dest;
end

-- concatenate two arrays
function array.join(dest, src)
  for ii = 1, #src do
    dest[#dest+1] = src[ii];
  end
  return dest;
end

-- flatten arbitrarily nested array
function array.flatten(t)
  -- I think this will be faster than having _flatten return an array to be joined with dest
  local function _flatten(dest, src)
    for _,v in ipairs(src) do
      if type(v) == "table" then
        _flatten(dest, v);
      else
        dest[#dest+1] = v;
      end
    end
    return dest;
  end
  return _flatten({}, {t});
end


-- add metatable to t1 to pass unhandled derefs to t2
-- alternative to table.merge
function table.inherit(t1, t2)
  setmetatable(t1, {__index = t2});
  return t1;
end

-- first table has precedence
function table.union(t1, t2)
  return table.merge(table.copy(t2), t1);
end

-- TODO: if t is a function, assume it is an iterator generator to be used with generic for loop
-- Default is then equiv to: map(bind(pairs, t), fn)
-- the order of the args (table before fn) is predicated on the long length of function defs in Lua
-- fn can return key and value
function table.map(t, fn)
  local yy = {};
  for key,val in pairs(t) do
    local a, b = fn(val, key);
    if b == nil then
      yy[key] = a;
    else
      yy[a] = b;
    end
  end
  return yy;
end

function table.toarray(t, fn)
  local yy = {};
  for key,val in pairs(t) do
    table.insert(yy, fn(val, key));
  end
  return yy;
end

function array.map(t, fn)
  local yy = {};
  for ii = 1,#t do
    -- using table.insert won't work if fn can return nil
    local a, b = fn(t[ii], ii);
    if b == nil then
      yy[ii] = a;
    else
      yy[a] = b;
    end
  end
  return yy;
end

-- map multiple arrays
function array.mapn(fn, ...)
  local args = {...}
  if #args == 1 then
    return array.map(args[1], fn);
  else
    local yy, fargs = {}, {};
    local jj = 1;
    while true do
      for ii = 1,#args do
        -- using table.insert won't work if fn can return nil
        fargs[ii] = args[ii][jj];
      end
      if not table.any(fargs) then break; end
      local a, b = fn(unpack(fargs));
      if b == nil then
        yy[jj] = a;
      else
        yy[a] = b;
      end
      jj = jj + 1;
    end
    return yy;
  end
end

function array.zip(...)
  return array.mapn(function(...) return {...}; end, ...);
end

-- pass multiple consecutive table elements to a map function, i.e., if t = {1,2,3,4,5,6} and grouping = 3,
--  tuplemap returns the following table: {fn(1,2,3), fn(4,5,6)}
function array.tuplemap(t, fn, grouping)
  grouping = grouping or 2;
  local yy = {};
  local ii, pos = 1, 1;
  repeat
    local fargs = {};
    for jj = pos,pos+grouping-1 do
      table.insert(fargs, t[jj]);
    end
    local a, b = fn(unpack(fargs));
    if b == nil then
      yy[ii] = a;
    else
      yy[a] = b;
    end
    ii = ii + 1;
    pos = pos + grouping;
  until pos > #t
  return yy;
end

-- map in place ... useful?
function table.transform(t, fn)
  for key,val in pairs(t) do
    t[key] = fn(val, key);
  end
  return t;
end

function table.deep(tblfn, t, userfn)
  local deepfn;
  deepfn = function(k, ...)
    return type(k) == "table" and tblfn(k, deepfn) or userfn(k, ...);
  end
  return tblfn(t, deepfn);
end

-- apply fn to all scalars (i.e. non-tables) in data structure of
--  arbitrary depth
function table.deepmap(t, fn)
  if type(t) == "table" then
    local yy = {};
    for key,val in pairs(t) do
      yy[key] = table.deepmap(val, fn);
    end
    return yy;
  else
    return fn(t);
  end
end

--function dict.map(t, fn)
--  return table.map(t, function(_, x) fn(x); end);
--end

-- generalized transpose
-- given x, a table of tables, return a table y s.t. y[j][i] = x[i][j]
function table.transpose(t)
  local yy = {};
  for ii,ti in pairs(t) do
    for jj,val in pairs(ti) do
      if not yy[jj] then yy[jj] = {}; end
      yy[jj][ii] = val;
    end
  end
  return yy;
end

function array.transpose(t)
  local yy = {};
  for ii,ti in ipairs(t) do
    for jj,val in ipairs(ti) do
      if not yy[jj] then yy[jj] = {}; end
      yy[jj][ii] = val;
    end
  end
  return yy;
end


--- This function indicates that we should use OOP and inheritance for
---  table and array
-- apply every function in table to argument(s) and return table of
--  results; converse of map()
function table.pam(fns, ...)
  local uparg = {...};
  return table.map(fns, function (fn) return fn(unpack(uparg)); end);
end

-- like vectorize(fn, 0), but for table
function table.vectorize(fn)
  return bindr(table.map, fn);
end

-- convert a table (key = val) to a dictionary (val = true)
function table.todict(t)
  local yy = {};
  for key,val in pairs(t) do
    yy[val] = true;
  end
  return yy;
end

function table.values(t)
  local yy = {};
  for key,val in pairs(t) do
    table.insert(yy, val);
  end
  return yy;
end

function table.keys(t)
  local yy = {};
  for key,val in pairs(t) do
    table.insert(yy, key);
  end
  return yy;
end

function table.filter(t, fn)
  local yy = {};
  fn = fn or function(v) return v; end;
  for key,val in pairs(t) do
    if fn(val,key) then
      yy[key] = val;
    end
  end
  return yy;
end

function array.filter(t, fn)
  local yy = {};
  for key,val in ipairs(t) do
    if fn(val,key) then
      table.insert(yy, val);
    end
  end
  return yy;
end

-- reverse an array - doesn't make any sense for tables
function array.rev(t)
  local yy = {};
  for ii = 0,#t-1 do
    table.insert(yy, t[#t-ii]);
  end
end

-- reverse array in place
function array.revinplace(t)
  for ii = 1,math.floor(#t/2) do
    t[ii], t[#t-ii+1] = t[#t-ii+1], t[ii];
  end
  return t;
end

-- reduce(function, table)
-- e.g: reduce(operator.add, {1,2,3,4}) -> 10
function array.reduce(t, fn, initial)
  local yy = initial or t[1];
  for ii = (initial and 1 or 2),#t do
    yy = fn(yy, t[ii], ii)
  end
  return yy;
end

function table.reduce(t, fn, initial)
  local yy = initial or table.head(t);
  local key = initial and next(t) or next(t, next(t));
  while key do
    yy = fn(yy, t[key], key)
    key = next(t, key);
  end
  return yy;
end

-- number of elements in a table (# only works for arrays)
function table.count(t)
  local n = 0;
  for key,val in pairs(t) do
    n = n + 1;
  end
  return n;
end

-- sum and prod - no need for initial for these
function table.sum(t)
  local yy = 0;
  for key,val in pairs(t) do
    yy = yy + val;
  end
  return yy;
end

function table.prod(t)
  local yy = 1;
  for key,val in pairs(t) do
    yy = yy * val;
  end
  return yy;
end

-- shortcircuited version of reduce(t, function (x,y) return x or y; end)
function table.any(t, fn)
  fn = fn or function (x) return x; end;
  for key,val in pairs(t) do
    if fn(val) then return val or true; end
  end
  return false;
end

-- reduce(t, function (x,y) return x and y; end)
function table.all(t, fn)
  fn = fn or function (x) return x; end;
  for key,val in pairs(t) do
    if not fn(val) then return false; end
  end
  return true;
end

function table.find(t, val)
  for k,v in pairs(t) do
    if v == val then return k; end
  end
  return nil;
end
table.contains = table.find;

-- pairs and ipairs are comparable for Lua, ipairs can be much faster for LuaJIT
function array.find(t, val)
  for k,v in ipairs(t) do
    if v == val then return k; end
  end
  return nil;
end
array.indexof = array.find;

function array.range(t, start, stop)
  return {unpack(t, start, stop)};
end

function table.select(t, sel)
  -- filter(t, function (val) return contains(start, val); end)
  local yy = {};
  for key, val in pairs(sel) do
    yy[val] = t[val];
  end
end

-- select values from t based on the presence of (true) values
--  with same key in sel
function table.intersect(t, sel)
  -- filter(t, function(val,key) return start[key]; end)
  local yy = {};
  for key, val in pairs(t) do
    if sel[key] then
      yy[key] = t[key];
    end
  end
end

function array.select(t, sel)
  local yy = {};
  for key, val in ipairs(sel) do
    table.insert(yy, t[val]);
  end
end

function array.intersect(t, sel)
  local yy = {};
  for key, val in ipairs(t) do
    if sel[key] then
      table.insert(yy, t[key]);
    end
  end
end

function array.last(t)
  return t[#t];
end
--last = array.last;

function make_array_inherit_table()
  table.inherit(array, table);
end
