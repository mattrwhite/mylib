-- less used utility functions

-- create vectorized version of fn which applies fn to each argument beyond position nleft
-- we expect this would mostly be used for fns with no return value
function vectorize(fn, nleft)
  return (nleft and nleft > 0) and
    function(...)
      local args = {...};
      local yy = {};
      for ii = nleft+1,#args do
        -- hack would be needed to make table.insert work properly with fns that return false
        yy[ii-nleft] = fn(unpack(args,1,nleft), args[ii]);
      end
      return unpack(yy);
    end
  or
    function(...)
      -- this is just: return unpack(table.map({...}, fn));
      local args = {...};
      local yy = {};
      for ii = 1,#args do
        yy[ii] = fn(args[ii]);
      end
      return unpack(yy);
    end
end

-- iterators to eliminate nested/multiple for loops: cross(a,b): iterate over
--  all pairs of elements from a and b (direct prod)
-- dot(a,b): iterator over {a[1], b[1]}, {a[2], b[2]}, ...
-- Extend to include all fns from autovars?
-- From the Sisal language

--[[
function cross(a, b)
  local function crossiter (t, keys)
    keys[2] = next(b, keys[2]);
    if not keys[2] then
      keys[1] = next(a, keys[1]);
      if not keys[1] then
        keys = nil;
      else
        keys[2] = next(b);
      end
    end
    return keys, a[ keys[1] ], b[ keys[2] ];
  end
  return crossiter, {a, b}, {next(a), next(b)};
end
--]]

-- What about being able to pass a name to sow(); reap() would then return
--  a struct instead of a (comma-separated) list - or we pass reap() a set of
--  names to be retrieved
-- On the other hand, we can just create multiple reaper() objects
function reaper()
  -- reap/sow - useful for capturing values from expressions, probing other
  --  functions (when used in a function passed to target function)
  -- Not sure how useful this will be - try it out.
  local this = {};
  local crop = {};

  -- note that these functions are global
  function this.reap(...)
    -- get sowed data
    local temp = crop;
    crop = {};
    -- only the first arg will be returned - other ideas?
    if #{...} > 0 then
      return ..., temp;  --unpack(temp);
    else
      return temp;  --unpack(temp);
    end
  end

  -- it seems the named (sowas) case would be more common, so call that sow
  --  and this sowanon, sownext, ???
  function this.sow(...)
    return sowas(#crop+1, ...);
  end

  -- keep track of order of calls to sow in another table?
  function this.sowas(tag, ...)
    arg = {...};
    -- store arg
    if #arg > 1 then
      crop[tag] = arg;
    else
      -- unpack works for #arg = 0 or 1
      crop[tag] = unpack(arg);
    end
    -- pass through
    return ...;
  end

  --return reap,sow;
  return this;
end

-- template processor
-- lines beginning with $$ are executed as lua; on other lines, $(<lua code>) is replaced with the result
--  of evaluating the enclosed lua code (consider ${} instead of $())
-- second argument to loadtemplate/dotemplate is environment to run in
-- from: http://lua-users.org/wiki/SlightlyLessSimpleLuaPreprocessor

function loadtemplate(chunk, env, name)
  local function parseDollarParen(pieces, chunk, s, e)
    local s = 1
    for term, executed, e in string.gfind(chunk, "()$(%b())()") do
      table.insert(pieces, string.format("%q..(%s or '')..",
        string.sub(chunk, s, term - 1), executed))
      s = e
    end
    table.insert(pieces, string.format("%q", string.sub(chunk, s)))
  end

  local function parseHashLines(chunk)
    local pieces, s, args = string.find(chunk, "^\n*$$ARGS%s*(%b())[ \t]*\n")
    if not args or string.find(args, "^%(%s*%)$") then
      pieces, s = {"return function(_put) ", n = 1}, s or 1
     else
      pieces = {"return function(_put, ", string.sub(args, 2), n = 2}
    end
    while true do
      local ss, e, lua = string.find(chunk, "^$$+([^\n]*\n?)", s)
      if not e then
        ss, e, lua = string.find(chunk, "\n$$+([^\n]*\n?)", s)
        table.insert(pieces, "_put(")
        parseDollarParen(pieces, string.sub(chunk, s, ss))
        table.insert(pieces, ")")
        if not e then break end
      end
      table.insert(pieces, lua)
      s = e + 1
    end
    table.insert(pieces, " end")
    return table.concat(pieces)
  end

  local fn, msg = loadstring(parseHashLines(chunk), name);
  if not fn then return fn, msg; end
  return setfenv(fn, env or _G)();
end

function dotemplate(...)
  local res = {};
  local fn, ok, msg
  fn, msg = loadtemplate(...);
  if not fn then return fn, msg; end
  ok, msg = pcall(fn, function(s) table.insert(res, s); end);
  if not ok then return ok, msg; end
  return table.concat(res);
end

-- dump table contents ... old version
--[[
dumptable = {};
function dumptable(t, name, dumpstack)
  if not name then
    name = "table";
  end

  local tostr = function(s)
    return type(s) == "string" and '"'..s..'"' or tostring(s);
  end

  -- prevent infinite recursion for self-referential tables
  if not dumpstack then
    dumpstack = {};
  end
  dumpstack[t] = true;

  for key,val in pairs(t) do
    if type(val) == "table" and not dumpstack[val] then
      dumptable(val, name.."["..tostr(key).."]", dumpstack);
    else
      print(name.."["..tostr(key).."] = "..tostr(val));
    end
  end
  dumpstack[t] = nil;
end
dump = dumptable; -- alias
]]
