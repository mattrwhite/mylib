-- lua-breakpoint

local _path = (...):match("(.-)[^%.%/]+$") or "";  -- module is passed its file path
require(_path..'utils');

-- use _,x = debug.getlocal(level,ii) to put the local into a variable
function locals(level)
  level = level or 2;
  local info = debug.getinfo(level, "f");
  if not info then return false; end
  print("local variables for function "..(info.name or "?").." at level "..level..":");
  for ii = 1, 100 do
    local name, val = debug.getlocal(level, ii);
    if not name then break; end
    print("  local "..level..","..ii..": "..name.." = "..to_string(val))
  end
  return true;
end

function breakpoint(printlocals, prompt)
  if printlocals then
    for level = 2, 11 do
      if not locals(level) then break; end
    end
  end

  -- the repl
  prompt = prompt or "DEBUG> ";
  io.write(prompt);
  for line in io.lines() do
    if line == "exit" or line == "quit" or line == "cont" then break; end
    if not string.find(line, ";%s*$") then
      line = "return "..line;
    end
    local chunk, err = loadstring(line);
    if chunk then
      local res = table.pack(pcall(chunk));
      if res[1] then
        table.remove(res, 1);
        print(to_string(table.unpack(res)));
      else
        print("Error running chunk: "..res[2]);
      end
    else
      print("Error loading chunk: "..err);
    end
    io.write(prompt);
  end
end

-- from http://snippets.luacode.org/snippets/title_88
function DEBUG()
  local G = _G;
  local locals = {};
  local max = 1000000;
  for l=2, max do
    local info = debug.getinfo(l, "f");
    if not info then break; end
    for i=1, max do
      local name, val = debug.getlocal(l, i);
      if not name then break; end
      if locals[name] then
        -- TODO: print level and index for both!
        print("Warning: multiple locals named "..name);
      else
        locals[name] = { where = l + 3, index = i, val = val, set = debug.setlocal }
      end
    end
    local f = info.func;
    for i=1, max do
      local name, val = debug.getupvalue(f, i);
      if not name then break; end
      if locals[name] then
        print("Warning: multiple locals named "..name);
      else
        locals[name] = locals[name] or { where = f, index = i, val = val, set = debug.setupvalue }
      end
    end
  end

  local ENV = setmetatable({}, {
    __index = function(t,k)
      if locals[k] then
        return locals[k].val;
      else
        return G[k];
      end
    end,
    __newindex = function(t,k,v)
      if locals[k] then
        local t = locals[k];
        t.val = v;
        t.set(t.where, t.index, v);
      else
        G[k] = v;
      end
    end
  })

  local function pcallshift(ok, ...)
    return ok, to_string(...);
  end

  local input;
  while true do
    io.write('DEBUG> ');
    input = io.read();
    if input == "exit" or input == "quit" or input == "cont" then break; end
    if not string.find(input, ";%s*$") then
      input = "return "..input;
    end
    local f, err = loadstring(input, "debug");
    if not f then
      print('Error loading chunk:', err);
    else
      local ok, res = pcallshift(pcall(setfenv(f, ENV)));
      if ok then
        print(res);
      else
        print('Error executing chunk:', err);
      end
      --local ok, err = pcall(setfenv(f, ENV));
      --if not ok then print('Error executing chunk:', err); end
    end
  end
end

--[[
function testfn()
  local x = 10;
  local y = 2.5;
  local z = x*3;
  --breakpoint();
  --debug.debug();
  DEBUG();
  z = z + y*y;
  print("z = "..z);
end

testfn();
]]
